/**
 * @file
 * Init WoW.
 */

/* eslint no-undef: 0 */

(function () {

  "use strict";

  new WOW().init();

})();
